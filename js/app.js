var QW = QW || {};
QW.State = QW.Lib.DataProvider.get('State') || {};
QW.State.Status = QW.State.Status || '';
QW.State.Char = QW.State.Char || {};
QW.State.Saves = QW.State.Saves || [];

/** UI bindings **/
$('body').on('click', '#btn_new', function(e){
  //alert('new');
  QW.State.Status = 'start';
  
  location.hash = '#charsheet'
  e.preventDefault();
});

$('body').on('click', '#btn_charsheet_done', function(e){
    console.log('save');
    QW.State.Char.name = $('#char_name').val();
    e.preventDefault();
});

$('body').on('change', '#char_race', function(e){
    console.log('chosen race: ' + $(this).val());
});

$('body').on('change', '#char_class', function(e){
    console.log('chosen class: ' + $(this).val());
});


QW.route = function (){
  if(QW.State.Status === 'idle'){
	//location.hash = '#home';
  }
  var hash = location.hash.length > 1 ? location.hash.substr(1) : 'home';
  var cont = hash[0].toUpperCase() + hash.substr(1);
  
  if(typeof QW.Controllers[cont]==='function'){
	console.log("running: " + cont);
	QW.Controllers[cont]();
  }
}

QW.Controllers = QW.Controllers || {};
QW.Controllers.Home = function(){
  var tpl = Handlebars.compile($('#tpl_mainmenu').html());
  console.log("this is Home");
  $('section').hide(function(){
	$('#mainmenu').html(tpl()).show();
  });
};
QW.Controllers.Charsheet = function(){
  var tpl = Handlebars.compile($('#tpl_charsheet').html());
  console.log('this is char');
  var params = {
  "char":new QW.Lib.Char(),
	"fullform":true
  }
  $('section').hide(function(){
	  $('#char').html(tpl(params)).show();
  });
};


(function($){
  window.addEventListener('hashchange', QW.route);
  console.log('Hash: ' + location.hash);
  QW.route();
  
  


  function update(timestamp){
	window.requestAnimationFrame(update);
  }
  //window.requestAnimationFrame(update);
})(jQuery);