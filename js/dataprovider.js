var QW = QW || {};
QW.Lib = QW.Lib || {};
QW.Lib.DataProvider = {
  ns:"QW",
  get:function(key){
	return JSON.parse(localStorage.getItem(this.ns + '.' + key));
  },
  set:function(key, value){
	localStorage.setItem(this.ns + '.' + key, JSON.stringify(value));
  },
  getAll:function(){
	var all = {};
	all[this.ns] = {};
	var keys = Object.keys(localStorage)
	for(var i=0, len=keys.length; i<len; i++){
	  all[keys[i].substr((this.ns.length+1))] = (JSON.parse(localStorage.getItem(keys[i])));
	}
	return all;
  },
  delete:function(key){
	localStorage.removeItem(this.ns + '.' + key);
  }
};