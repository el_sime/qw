var QW = QW || {};

QW.Lib = QW.Lib || {};

QW.Lib.Char = function(){
	this.name = '';
	this.race = '';
	this.class = '';
	this.lvl = 1;
	this.xp = 0;
	this.hp = 0; //hit points
	this.ap = 0; //action points
	this.wp = 0; //will power points
	this.str = 0;
	this.agi = 0;
	this.wpw = 0;
	this.con = 0;
	
	this.setRace = function(race){
		switch(race){
			case 'elf':
				this.str += 1;
				this.agi += 2;
				this.wp  += 3;
				break;
			case 'human':
				this.str += 2;
				this.agi += 2;
				this.wp  += 1;
				this.con += 1;
				break;
			case 'dwarf':
				this.str += 3;
				this.agi += 1;
				this.wp  += 0;
				this.con += 2;
				break;
		}
	};
};